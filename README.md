Génération de Cours.php et Salle.php à l'aide de make:entity.
Création des méthodes 'construct', 'toString' et 'toArray' pour ces 2 entités.
Ajout des méthodes 'addCour' et 'removeCour' dans l'entité cours.
Ajout d'un attribut 'cours' et de la méthode 'addCours' dans les entité Professeur et Matiere ainsi qu'une méthode 'removeCours' dans Professeur.

Génération de CoursController.php et SalleController.php dans controller/api
Implémentation des méthodes 'getCours' et 'getSalle' dans leur controller respectif.

Modification de 'edt.html' avec ajout des méthodes:

*  getCours() ;
*  getSalles() ;
*  setCoursDeDepart() qui permet de définir le cours de départ du calendrier ;
*  changementJour(action) qui passe au jour précédent ou suivant en fonction du paramêtre passé et met à jour la liste des cours
*  dateToUtilisateurFriendly() qui convertit une date au format string lisible par un utilisateur français

La gestion côté Symfony est restée relativement simple et s'est déroulée sans blocages particuliers exceptés quelques manipulations avec la BD sur deux postes différents.

Côté VueJS par contre, nous avons enchainé blocages sur blocages du fait de notre non-maitrise du VueJS. l'utilisation de bibliothèques annexes a été une source d'ennuis initiaux mais qui ont pu être résolus par la suite mais qui nous ont fait perdre beaucoup de temps.

La navigation entre les différents jours est proche du fonctionnel mais l'affichage est encore trop imparfait et n'affiche pas tous les éléments comme ceux du jour de départ.