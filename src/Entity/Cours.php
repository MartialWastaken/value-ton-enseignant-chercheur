<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
* @ORM\Entity(repositoryClass="App\Repository\CoursRepository")
*/
class Cours
{
  // Liste des différents types de cours
  const TYPES = ['TP', 'TD', 'Cours'];

  // ----- Données membres ----- //

  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * @ORM\Column(type="datetime")
  * @Assert\Type("\DateTime")
  * @Assert\GreaterThan("now", message="La date choisie ne peut d'aujourd'hui")
  */
  private $dateHeureDebut;

  /**
  * @ORM\Column(type="datetime")
  * @Assert\Type("\DateTime")
  * @Assert\GreaterThan(propertyPath="dateHeureDebut", message="La date de fin doit être supérieure celle de début")
  *
  */
  private $dateHeureFin;

  /**
  * @ORM\Column(type="string", length=15)
  * @Assert\Choice(choices=Cours::TYPES)
  * @Assert\NotBlank
  */
  private $type;

  /**
  * Un cours est associé à un professeur unique
  * @ORM\ManyToOne(targetEntity="App\Entity\Professeur", inversedBy="cours")
  * @ORM\JoinColumn(nullable=false)
  */
  private $professeur;

  /**
  * Un cours est associé à une seule salle
  * @ORM\ManyToOne(targetEntity="App\Entity\Salle", inversedBy="cours")
  * @ORM\JoinColumn(nullable=false)
  */
  private $salle;

  /**
  * Un cours est associé à une seule matière
  * @ORM\ManyToOne(targetEntity="App\Entity\Matiere", inversedBy="cours", cascade="remove")
  * @ORM\JoinColumn(nullable=false)

  */
  private $matiere;

  // ----- Getters ----- //

  public function getId(): ?int { return $this->id; }
  public function getDateHeureDebut(): ?\DateTimeInterface { return $this->dateHeureDebut; }
  public function getDateHeureFin(): ?\DateTimeInterface { return $this->dateHeureFin; }
  public function getType(): ?string { return $this->type; }
  public function getProfesseur(): ?Professeur {return $this->professeur; }
  public function getSalle(): ?Salle {return $this->salle;}
  public function getMatiere(): ?Matiere {return $this->matiere;}

  // ----- Setters ----- //

  public function setDateHeureDebut(\DateTimeInterface $dateHeureDebut): self {
    $this->dateHeureDebut = $dateHeureDebut;
    return $this;
  }

  public function setDateHeureFin(\DateTimeInterface $dateHeureFin): self {
    $this->dateHeureFin = $dateHeureFin;
    return $this;
  }

  public function setType(string $type): self {
    $this->type = $type;
    return $this;
  }

  public function setProfesseur(Professeur $professeur): self {
    $this->professeur = $professeur;
    return $this;
  }

  public function setSalle (Salle $salle):self {
    $this->salle = $salle;
    return $this;
  }

  public function setMatiere (Matiere $matiere):self {
    $this->matiere = $matiere;
    return $this;
  }

  // ----- Constructeurs ----- //

  /**
  * Constructeur permettant definir à la date du jour le formulaire (au lieu de celle par défaut du 1 janvier 2015)
  */
  public function __construct(){
    $this->dateHeureDebut = new \DateTime();
    $this->dateHeureFin = new \DateTime();
  }

  // ----- Méthodes ----- //

  /**
  * Renvoie une chaine décrivant Cours
  */
  public function __toString(){
    return $this->type . ' - ' . $this->professeur . ' (Salle : ' . $this->salle . ')';
  }

  /**
  * Renvoie un tableau listant les attributs de Cours
  */
  public function toArray(){
    return [
      'id' => $this->getId(),
      'dateHeureDebut' => $this->getDateHeureDebut()->format('Y-m-d H:i'),
      'dateHeureFin' => $this->getDateHeureFin()->format('Y-m-d H:i'),
      'type' => $this->getType(),
      'professeur' => $this->getProfesseur()->toArray(),
      'salle' => $this->getSalle()->toArray(),
      'matiere' => $this->getMatiere()->toArray()
    ];
  }
}
