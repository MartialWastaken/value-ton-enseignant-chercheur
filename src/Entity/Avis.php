<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* @ORM\Entity(repositoryClass="App\Repository\AvisRepository")
* @UniqueEntity(
* fields={"professeur", "emailEtudiant"},
* errorPath="emailEtudiant",
* message="Cet étudiant a déjà noté ce professeur"
* )
*/
class Avis
{

  // ----- Propriétés ----- //

  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;
  /**
  * @ORM\Column(type="integer")
  * @Assert\Range(min=0, max=5)
  */
  private $note;
  /**
  * @ORM\Column(type="text")
  */
  private $commentaire;
  /**
  * @ORM\Column(type="string")
  */
  private $emailEtudiant;
  /**
  * @ORM\ManyToOne(targetEntity="App\Entity\Professeur", inversedBy="avis")
  * @ORM\JoinColumn(nullable=false)
  */
  private $professeur;

  // ----- Constructeurs ----- //

  // ----- Méthodes ----- //

  // Renvoie une chaine décrivant Avis
  public function __toString(){
    return $this->note . ' - '. $this->commentaire . ' (Soumis par : '. $this->emailEtudiant . ')';
  }

  // Renvoie un tableau listant les attributs de Avis
  public function toArray(){
    return [
      'id' => $this->getId(),
      'note' => $this->getNote(),
      'commentaire' => $this->getCommentaire(),
      'emailEtudiant' => $this->getEmailEtudiant()
    ];
  }

  // ----- Getters ----- //
  public function getId(): ?int { return $this->id; }
  public function getNote(): ?int { return $this->note; }
  public function getCommentaire(): ?string { return $this->commentaire; }
  public function getEmailEtudiant(): ?string { return $this->emailEtudiant; }
  public function getProfesseur(): ?Professeur { return $this->professeur; }

  // ----- Setters ----- //
  public function setNote(int $note): self {
    $this->note = $note;
    return $this;
  }

  public function setCommentaire(string $commentaire): self {
    $this->commentaire = $commentaire;
    return $this;
  }

  public function setEmailEtudiant(string $emailEtudiant): self {
    $this->emailEtudiant = $emailEtudiant;
    return $this;
  }

  public function setProfesseur(?Professeur $professeur): self {
    $this->professeur = $professeur;
    return $this;
  }
}
