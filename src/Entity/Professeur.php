<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
* @ORM\Entity(repositoryClass="App\Repository\ProfesseurRepository")
*/
class Professeur
{

  // ----- Propriétés ----- //

  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;
  /**
  * @ORM\Column(type="string")
  */
  private $nom;

  /**
  * @ORM\Column(type="string")
  */
  private $prenom;

  /**
  * @ORM\Column(type="string", unique=true)
  */
  private $email;

  /**
  * @ORM\ManyToMany(targetEntity="App\Entity\Matiere", inversedBy="professeurs", cascade="remove")
  */
  private $matieres;

  /**
  * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="professeur")
  */
  private $avis;

  /**
  * Un professeur peut avoir 0 ou plusieurs cours
  * @ORM\OneToMany(targetEntity="App\Entity\Cours", mappedBy="professeur")
  */
  private $cours;

  // ----- Constructeurs ----- //

  // Constructeur par défaut permettant de créer une collection d'avis et matières vides
  public function __construct(){
    $this->matieres = new ArrayCollection();
    $this->avis = new ArrayCollection();
  }

  // ----- Méthodes ----- //

  // Renvoie une chaine décrivant Professeur
  public function __toString(){
    return $this->prenom . ' ' . $this->nom;
  }

  // Renvoie un tableau listant les attributs de Professeur
  public function toArray(){
    return [
      'id' => $this->getId(),
      'nom' => $this->getNom(),
      'prenom' => $this->getPrenom(),
      'email' => $this->getEmail(),
      'matieres' => array_map(function ($matiere) {
        return $matiere->toArray();
      }, $this->getMatieres()->toArray()),
      'avis' => array_map(function ($avis) {
        return $avis->toArray();
      }, $this->getAvis()->toArray())
    ];
  }

  // ----- Getters ----- //
  public function getId(): ?int { return $this->id;}
  public function getNom(): ?string { return $this->nom; }
  public function getPrenom(): ?string { return $this->prenom;}
  public function getEmail(): ?string { return $this->email;}
  public function getMatieres(): ?Collection { return $this->matieres;}
  public function getAvis(): ?Collection { return $this->avis;}
  public function getCours(): ?Collection {return $this->cours;}

  // ----- Setters ----- //
  public function setNom(string $nom): self {
    $this->nom = $nom;
    return $this;
  }

  public function setPrenom(string $prenom): self {
    $this->prenom = $prenom;
    return $this;
  }

  public function setEmail(string $email): self {
    $this->email = $email;
    return $this;
  }

  // Ajoute une matière à la liste de celles enseignées si celle donnée n'existe pas
  public function addMatiere(Matiere $matiere): self {
    if (!$this->matieres->contains($matiere)) {
      $this->matieres[] = $matiere;
    }

    return $this;
  }

  // Supprime la matière de celles enseignées si celle donnée existe
  public function removeMatiere(Matiere $matiere): self {
    if ($this->matieres->contains($matiere)) {
      $this->matieres->removeElement($matiere);
    }

    return $this;
  }

  // Ajoute un avis à la liste de ceux renseignés si celui donné n'existe pas
  public function addAvis(Avis $avis): self {
    if (!$this->avis->contains($avis)){
      $this->avis[] = $avis;
      $avis->setProfesseur($this);
    }

    return $this;
  }

  // Supprime un avis à la liste de ceux renseignés si celui donné existe
  public function removeAvis(Avis $avis): self {
    if ($this->avis->contains($avis)) {
      $this->avis->removeElement($avis);

      // set the owning side to null (unless already changed)
      if ($avis->getProfesseur() === $this) {
        $avis->setProfesseur(null);
      }
    }

    return $this;
  }

  /**
  * Ajoute un cours s'il n'existe pas déjà
  */
  public function addCour(Cours $cours): self{
    if (!$this->cours->contains($cours)) {
      $this->cours[] = $cours;
      $cours->setProfesseur($this);
    }

    return $this;
  }

  /**
  * Supprime un cours s'il est dans la liste
  */
  public function removeCour(Cours $cours): self{
    if ($this->cours->contains($cours)) {
      $this->cours->removeElement($cours);
    }

    // Suppression de la liaison avec le cours
    if ($cours->getProfesseur() === $this) {
      $cours->setProfesseur(null);
    }

    return $this;
  }


}
