<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass="App\Repository\MatiereRepository")
*/
class Matiere
{

  // ----- Propriétés ----- //

  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * @ORM\Column(type="string")
  */
  private $titre;

  /**
  * @ORM\Column(type="string")
  */
  private $reference;

  /**
  * @ORM\ManyToMany(targetEntity="App\Entity\Professeur", mappedBy="matieres")
  */
  private $professeurs;

  /**
  * @ORM\OneToMany(targetEntity="App\Entity\Cours", mappedBy="matiere")
  */
  private $cours;

  // ----- Constructeurs ----- //

  // Constructeur initialisant un ArrayCollection
  public function __construct() {
    $this->professeurs = new ArrayCollection();
  }

  // Renvoie une chaine décrivant Professeur
  public function __toString(){
    return $this->titre . ' - ' . $this->reference;
  }

  // Renvoie un tableau listant les attributs de Matiere
  public function toArray(){
    return [
      'id' => $this->getId(),
      'titre' => $this->getTitre(),
      'reference' => $this->getReference()
    ];
  }

  // ----- Getters ----- //
  public function getId(): ?int { return $this->id; }
  public function getTitre(): ?string { return $this->titre; }
  public function getReference(): ?string { return $this->reference; }
  public function getCours(): ?Collection { return $this->cours;}

  /**
  * @return Collection|Professeur[]
  */
  public function getProfesseurs(): Collection { return $this->professeurs; }

  // ----- Setters ----- //
  public function setTitre(string $titre): self {
    $this->titre = $titre;
    return $this;
  }

  public function setReference(string $reference): self {
    $this->reference = $reference;
    return $this;
  }

  // ----- Méthodes ----- //

  /**
  * Ajoute un professeur à la liste
  */
  public function addProfesseur(Professeur $professeur): self {
    if (!$this->professeurs->contains($professeur)) {
      $this->professeurs[] = $professeur;
      $professeur->addMatiere($this);
    }

    return $this;
  }

  /**
  * Supprime un professeur de la liste
  */
  public function removeProfesseur(Professeur $professeur): self {
    if ($this->professeurs->contains($professeur)) {
      $this->professeurs->removeElement($professeur);
      $professeur->removeMatiere($this);
    }

    return $this;
  }

  /**
  * Ajoute un cours à la liste
  */
  public function addCours(Cours $cours): self {
    if (!$this->cours->contains($cours)) {
      $this->cours[] = $cours;
      $cours->addMatiere($this);
    }

    return $this;
  }
}
