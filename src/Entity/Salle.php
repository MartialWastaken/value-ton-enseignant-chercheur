<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* @ORM\Entity(repositoryClass="App\Repository\SalleRepository")
*/
class Salle
{

  // ----- Données membres ----- //

  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * @ORM\Column(type="integer")
  * @Assert\GreaterThan(value=0, message="Le numéro de salle doit être supérieur à 0")
  *
  */
  private $numero;

  /**
  * Une salle peut avoir plusieurs cours
  * @ORM\OneToMany(targetEntity="App\Entity\Cours", mappedBy="salle")
  */
  private $cours;

  // ----- Getters ----- //

  public function getId(): ?int { return $this->id; }
  public function getNumero(): ?int {return $this->numero; }
  public function getCours(): ?Collection {return $this->cours;}

  // ----- Setters ----- //

  public function setNumero(int $numero): self {
    $this->numero = $numero;
    return $this;
  }


  // ----- Constructeurs ----- //

  // Constructeur par défaut permettant de créer une collection de cours vide
  public function __construct(){
    $this->cours = new ArrayCollection();
  }

  // ----- Méthodes ----- //

  /**
  * Renvoie une chaine décrivant Salle
  */
  public function __toString(){
    return strval($this->numero);
  }

  /**
  * Renvoie un tableau listant les attributs de Salle
  */
  public function toArray(){
    return [
      'id' => $this->getId(),
      'numero' => $this->getNumero()
    ];
  }

  /**
  * Ajoute un cours à la Salle s'il n'existe pas déjà
  */
  public function addCour(Cours $cours): self{
    if (!$this->cours->contains($cours)) {
      $this->cours[] = $cours;
      $cours->setSalle($this);
    }

    return $this;
  }

  /**
  * Supprime un cours s'il est dans la liste
  */
  public function removeCour(Cours $cours): self{
    if ($this->cours->contains($cours)) {
      $this->cours->removeElement($cours);
    }

    // Suppression de la liaison avec le cours
    if ($cours->getProfesseur() === $this) {
      $cours->setProfesseur(null);
    }

    return $this;
  }
}
