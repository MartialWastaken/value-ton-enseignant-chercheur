<?php

namespace App\Controller\Api;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Cours;

/**
* @Route("/api")
*/
class CoursController extends AbstractController
{
    /**
     * @Route("/cours", name="api_cours")
     */
     public function getCours(EntityManagerInterface $em) {
         // Récupération de toutes les salles
         $cours = $em->getRepository(Cours::class)->findAll();

         return $this->json(array_map(function ($cour) {
           return $cour->toArray();
         }, $cours));
     }
}
