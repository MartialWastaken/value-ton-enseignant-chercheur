<?php

namespace App\Controller\Api;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Salle;

/**
* @Route("/api")
*/
class SalleController extends AbstractController
{
    /**
     * @Route("/salles", name="api_salles")
     */
    public function getSalle(EntityManagerInterface $em) {
        // Récupération de toutes les salles
        $salles = $em->getRepository(Salle::class)->findAll();

        return $this->json(array_map(function ($salle) {
          return $salle->toArray();
        }, $salles));
    }
}
