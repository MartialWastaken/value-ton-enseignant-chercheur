<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use App\Entity\Professeur;
use App\Entity\Avis;
use App\Form\AvisType;

use Doctrine\ORM\EntityManagerInterface;

/**
* @Route("/api")
*/
class ProfesseurController extends AbstractController
{
  /**
  * @Route("/professeurs", name="api_professeurs")
  */
  public function getProfesseurs(EntityManagerInterface $em){
    // Récupération de tous les professeurs
    $professeurs = $em->getRepository(Professeur::class)->findAll();

    return $this->json(array_map(function ($professeur) {
      return $professeur->toArray();
    }, $professeurs));

    /* Inscription des professeurs dans un tableau
    $tableauProfesseurs = [];
    foreach ($professeurs as $professeur) {
    $tableauProfesseurs[] = $professeur->toArray();
  }

  /* Création de la réponse
  $reponse = new Response();
  $reponse->headers->set('Content-type', 'application/json; charset=utf-8');
  $reponse->setContent(json_encode($professeurs));
  $reponse->setStatusCode(200);

  return $this->json($tableauProfesseurs);
  */

}

/**
* @Route("/professeur/{id}", name="api_professeur")
*/
public function getProfesseurById(EntityManagerInterface $em, Professeur $professeur){
  return $this->json($professeur->toArray());

}

/**
* @Route("/professeur/{id}/avis", name="api_professeur_avis", methods={"GET"})
*/
public function getAvisProfesseurById(EntityManagerInterface $em, Professeur $professeur){

  // Récupération des avis du professeur et insertion dans un tableau
  $listeAvis = $professeur->getAvis()->toArray();

  return $this->json(array_map(function ($avis) {
    return $avis->toArray();
  }, $listeAvis));

}

/**
* Supprime un avis à partir de son Id
*
* @Route("/avis/{id}", name="api_professeur_remove_avis", methods={"DELETE"})
*/
public function removeAvisById(EntityManagerInterface $em, Avis $avis){
  // Suppression et MàJ en base
  $em->remove($avis);
  $em->flush();

  return $this->json(null, 204);

}

/**
* Renvoie un avis
*
* @Route("/avis/{id}", name="get_avis", methods={"GET"})
*/
public function getAvis(Avis $avis){

  return $this->json($avis->toArray());

}

/**
* Crée un avis et l'ajoute à un professeur
*
* @Route("/professeur/{id}/avis", name="api_put_professeur_avis", methods={"PUT"})
*/
public function putProfesseurAvis(EntityManagerInterface $em, Professeur $professeur, Request $request, ValidatorInterface $validator){

  // Récupération du contenu de la requête
  $data = json_decode($request->getContent(), true);
  $avis = (new Avis)->setProfesseur($professeur);
  $form = $this->createForm(AvisType::class, $avis, ['csrf protection' => false]);
  $form->submit($data);

  if (! $form->isValid()) {
    return $this->json((string) $form->getErrors(), 400);
  }

  /*
  // Création de l'avis à partir des données reçues
  $avis = new Avis;
  $avis->setNote($data->note)
  ->setCommentaire($data->commentaire)
  ->setEmailEtudiant($data->emailEtudiant)
  ->setProfesseur($professeur);

  // Vérification que les données sont conformes
  $erreurs = $validator->validate($avis);

  // Si non, renvoi d'une erreur
  if(count($erreurs)){
  return $this->json(['message' => 'Avis invalide'], 400);
}

*/

$avis = $form->getData();
// Ajout et MàJ en BD
$em->persist($avis);
$em->flush();

return $this->json(null, 200);
}


}
