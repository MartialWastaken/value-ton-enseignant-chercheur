<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Professeur;
use App\Form\ProfesseurType;

/**
* @Route("/professeurs")
*/
class ProfesseurController extends AbstractController
{
  /**
  * @Route("/", name="professeurs")
  */
  public function index() {
    // Plus communément appelé $em
    $entityManager = $this->getDoctrine()->getManager();
    $professeurs = $entityManager->getRepository(Professeur::class)->findAll();

    // Renvoi à la vue avec la liste des professeurs
    return $this->render('professeur/index.html.twig', [
      'listeProfesseurs' => $professeurs
    ]);
  }

  /**
  * @Route("/create", name="professeursCreate", methods={"GET", "POST"})
  */
  public function create(Request $request, EntityManagerInterface $em) {
    // Création d'un Professeur et envoi du formulaire
    $professeur = new Professeur;
    $form = $this->createForm(ProfesseurType::class, $professeur);

    // Récupération du formulaire
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      // MàJ en BD
      $em->persist($professeur);
      $em->flush();

      // Renvoi à la vue avec le formulaire
      return $this->redirectToRoute('professeurs');
    }

    return $this->render('professeur/create.html.twig', [
      'form' =>$form->createView()
    ]);
  }
  /**
  * @Route("/{id}/edit", name="professeursEdit", methods={"GET", "POST"})
  */
  public function edit(Request $request, EntityManagerInterface $em, int $id) {

    $em = $this->getDoctrine()->getManager();
    $professeur = $em->getRepository(Professeur::class)->find($id);
    $form = $this->createForm(ProfesseurType::class, $professeur);

    // Récupération du formulaire
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      // MàJ en BD
      $em->persist($professeur);
      $em->flush();

      // Renvoi à la vue avec le formulaire
      return $this->redirectToRoute('professeurs');
    }

    return $this->render('professeur/create.html.twig', [
      'form' =>$form->createView()
    ]);
  }

  /**
  * @Route("/{id}/remove", name="professeursRemove", methods={"GET", "POST"})
  */
  public function remove(Request $request, Professeur $id, EntityManagerInterface $em){
    // $professeur = $em->getRepository(Professeur::class)->findById($id);
    $em->remove($id);
    $em->flush();

    // Renvoi à la vue avec la liste des professeurs
    return $this->redirectToRoute('professeurs');
  }
}
